package com.example.kristjan.eventreminder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by kristjan on 2.1.2017.
 */

public class Methods {
    public static int userid=0; //id od uprabnika (za vpis v bazo od uid)
    public static String MD5hash(String s){ // MD5 za gesla
        String a="";
        try {
            byte[] bytes = s.getBytes("UTF-8");

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytes);
            //a=thedigest.toString();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                sb.append(Integer.toString((thedigest[i] & 0xff) + 0x100, 16).substring(1));
            }
            a=sb.toString();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return a;
    }
}
