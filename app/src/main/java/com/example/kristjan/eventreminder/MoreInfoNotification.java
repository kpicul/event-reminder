package com.example.kristjan.eventreminder;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MoreInfoNotification extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info_notification);
        Bundle extras = getIntent().getExtras();
        String nTitle = extras.getString("title");
        String nTime = extras.getString("time");
        String nDate = extras.getString("date");
        String nText = extras.getString("text");
        final String nLocation = extras.getString("location");


        TextView title1=(TextView) findViewById(R.id.twTitle);
        TextView time1=(TextView) findViewById(R.id.twTime);
        TextView date1=(TextView) findViewById(R.id.twDate);
        TextView text1=(TextView) findViewById(R.id.twText);
        Button map1 = (Button) findViewById(R.id.bShowMap);

        date1.setText(nDate);
        text1.setText(nText);
        title1.setText(nTitle);
        time1.setText(nTime);


        map1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                /*Intent intent=new Intent(MoreInfoNotification.this,ShowMapLocationActivity.class);
                intent.putExtra("mapLat", nMapLat);
                intent.putExtra("mapLng", nMapLng);

                startActivity(intent);*/
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=1000, Večna pot 113, 1000 Ljubljana, Slovenia&daddr="+nLocation));
                startActivity(intent);
            }
        });
    }
}
