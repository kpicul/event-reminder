package com.example.kristjan.eventreminder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button back=(Button)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener(){ //klik na gumb back
            public void onClick(View v){
                Intent in=new Intent(Register.this,Login.class);
                startActivity(in);
            }
        });
        Button register=(Button)findViewById(R.id.reg);
        register.setOnClickListener(new View.OnClickListener(){ // klik na gumb za registracijo
            public void onClick(View v){
                EditText nam=(EditText)findViewById(R.id.regNam);
                EditText pass1=(EditText)findViewById(R.id.regPass);
                EditText pass2=(EditText)findViewById(R.id.regRep);
                if(pass1.getText().toString().equals(pass2.getText().toString())){
                    DBInsert(nam.getText().toString(),pass1.getText().toString());
                }
                else{
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(v.getContext());

                    dlgAlert.setMessage("Passwords do not match");
                    dlgAlert.setTitle("Error Message...");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                }
            }
        });
    }
    public void DBInsert(String name,String password){ //dodajanje uporabnika v bazo
        DatabaseConnector db=new DatabaseConnector(this);
        List ls=db.namesList();
        if(ls.contains(name)){
            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

            dlgAlert.setMessage("Username already exists");
            dlgAlert.setTitle("Error Message...");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
        else {
            db.adduser(name, password);
            Intent in=new Intent(Register.this,Login.class);
            startActivity(in);
        }
    }
}
