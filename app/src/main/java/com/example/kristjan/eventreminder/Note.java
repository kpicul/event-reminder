package com.example.kristjan.eventreminder;

/**
 * Created by kristjan on 28.12.2016.
 */

public class Note {
    String title,text,location,date,time; //naslov,vsebina,lokacija,datum,čas(ura)
    String id; //id nota v bazi (v tabeli events)
    public Note(String title,String text,String date,String time){ //konstruktor
        this.title=title;
        this.text=text;
        this.date=date;
        this.time=time;
    }
    // getterji in setterji
    public void setLoc(String loc){
        this.location=loc;
    }
    public void setTitle(String ntitle){
        this.title=title;
    }
    public void setText(String text){
        this.text=text;
    }
    public void setDate(String date){
        this.date=date;
    }
    public void setTime(String time){
        this.time=time;
    }
    public void setId(String id){
        this.id=id;
    }
    public String getTitle(){
        return this.title;
    }
    public String getText(){
        return this.text;
    }
    public String getLocation(){
        return this.location;
    }
    public String getDate(){
        return this.date;
    }
    public String getTime(){
        return this.time;
    }
    public String getId(){
        return this.id;
    }

}
