package com.example.kristjan.eventreminder;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button log=(Button)findViewById(R.id.log);
        Button reg=(Button)findViewById(R.id.reg);
        log.setOnClickListener(new View.OnClickListener(){ //klik na gumb za prijavo
            public void onClick(View v){
                EditText nam=(EditText)findViewById(R.id.uname);
                EditText pass=(EditText)findViewById(R.id.passw);
                DatabaseConnector db=new DatabaseConnector(v.getContext());
                Map<String, String> dict = db.passwords();
                Map <String,String> id=db.ids();
                try {
                    if (dict.get(nam.getText().toString()).equals(Methods.MD5hash(pass.getText().toString()))) {
                        Methods.userid = Integer.parseInt(id.get(nam.getText().toString()));
                        Intent in = new Intent(Login.this, Notes.class);
                        startActivity(in);
                    } else {
                        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(v.getContext());
                        dlgAlert.setMessage("wrong username or password");
                        dlgAlert.setTitle("Error Message...");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();
                    }
                }
                catch (Exception e){
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(v.getContext());
                    dlgAlert.setMessage("wrong username or password");
                    dlgAlert.setTitle("Error Message...");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                }
            }
        });

        reg.setOnClickListener(new View.OnClickListener(){ // klik na gumb za registracijo
            public void onClick(View v){
                Intent in=new Intent(Login.this,Register.class);
                startActivity(in);
            }
        });
    }

}
