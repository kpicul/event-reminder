package com.example.kristjan.eventreminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.w3c.dom.Text;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * TODO
 * delete button
 * eventi naj se razporedijo po času
 */

public class Addnote extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    boolean update=false;
    String id="0";
    NotificationManager notificationManager;
    int notifID = 11;
    boolean isNotificActive = false;
    private TextView get_place;
    int PLACE_PICKER_REQUEST = 1;
    private static final LatLngBounds BOUNDS = new LatLngBounds(new LatLng(46.046944, 14.461679), new LatLng(46.052648, 14.482333));
    int year, month, day, hour, minute;
    int yearFinal, monthFinal, dayFinal, hourFinal, minuteFinal;
    Note n;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnote);
        get_place = (EditText)findViewById(R.id.location);
        //TextView pickTime = (TextView)findViewById(R.id.twPickTime);
        EditText pickDate = (EditText) findViewById(R.id.date);

        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Addnote.this, Addnote.this, year, month, day);
                datePickerDialog.show();
            }
        });




        get_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();

                    intentBuilder.setLatLngBounds(BOUNDS);
                    Intent intent = intentBuilder.build(Addnote.this);
                    // Start the Intent by requesting a result, identified by a request code.
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        Button back=(Button)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener(){ //klik na gumb back
            public void onClick(View v){
                Intent in=new Intent(Addnote.this,Notes.class);
                startActivity(in);
            }
        });
        Button addnote=(Button)findViewById(R.id.add);

        addnote.setOnClickListener(new View.OnClickListener(){ //klik na gumb update,add
            public void onClick(View v){
                EditText title=(EditText)findViewById(R.id.title);
                EditText text=(EditText)findViewById(R.id.text);
                EditText date=(EditText)findViewById(R.id.date);
                //EditText time=(EditText)findViewById(R.id.time);
                EditText location=(EditText)findViewById(R.id.location);
                String []c=parser(date.getText().toString());
                if(!update){
                    n=new Note(title.getText().toString(),text.getText().toString(),c[0],c[1]);
                    n.setLoc(location.getText().toString());
                    DBInsert(n);

                    /**
                     * notifications
                     */
                    //setAlarm(v);
                }
                else {
                    n=new Note(title.getText().toString(),text.getText().toString(),c[0],c[1]);
                    n.setLoc(location.getText().toString());
                    n.setId(id);
                    DBUpdate(n);
                    update=false;

                    /**
                     * notifications
                     */

                }
                setAlarm(v);
            }
        });
        String myString = "";
        final Intent intent = getIntent(); // pridobivanje podatkov od nota (če gre za update ko klikne na event)
        if (intent != null) {
            final Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String title = bundle.getString("title");
                id = bundle.getString("id");
                String text = bundle.getString("text");
                String time = bundle.getString("time");
                String date = bundle.getString("date");
                String location = bundle.getString("location");
                String dat=datetime(date,time);
                EditText title1=(EditText)findViewById(R.id.title);
                EditText text1=(EditText)findViewById(R.id.text);
                EditText date1=(EditText)findViewById(R.id.date);
                //EditText time1=(EditText)findViewById(R.id.time);
                EditText location1=(EditText)findViewById(R.id.location);
                title1.setText(title);
                text1.setText(text);
                //time1.setText(time);
                date1.setText(dat);
                location1.setText(location);
                addnote.setText("Update");
                update=true;
                n=new Note(title,text,date,time);
                n.setLoc(location);
                n.setId(id);
                //back.setText(myString);
                if (myString == null) {
                    myString = "Privzeta vrednost";
                }
            }
        }
        Button delete=(Button)findViewById(R.id.delete);
        if(update) {
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new View.OnClickListener(){ //klik na gumb back
                public void onClick(View v){
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    DBDelete();
                                    Intent in=new Intent(Addnote.this,Notes.class);
                                    startActivity(in);

                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setMessage("Are you sure you want to delete event?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            });
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        EditText ed=(EditText)findViewById(R.id.location);
        if(requestCode==PLACE_PICKER_REQUEST){
            if(resultCode==RESULT_OK){
                Place place = PlacePicker.getPlace(data, this);
                String address = place.getAddress().toString();

                /**
                 * TODO
                 * namesto da povozi 'click to select place' naj se text napiše v pravilen edittext (mislim da ga imaš ti pod location)
                 */
                //get_place.setText(address);
                ed.setText(address);
            }
        }
    }

    public void DBInsert(Note note){ // dodajanje nota v bazo
        DatabaseConnector db=new DatabaseConnector(this);
        db.addevent(note);
        Intent in=new Intent(Addnote.this,Notes.class);
        startActivity(in);

    }
    public void DBUpdate(Note note){ // posodabljanje nota v bazi
        DatabaseConnector db=new DatabaseConnector(this);
        db.updateEvent(note);
        Intent in=new Intent(Addnote.this,Notes.class);
        startActivity(in);
    }

    public void stopNotification(View view) {
        // če hočemo manualno izbrisat notification; for potential future use - currently not needed
        if(isNotificActive) {
            notificationManager.cancel(notifID);
        }

    }
    public String datetime(String a,String b){
        String dat=a+" "+b;
        return dat;
    }

    public void setAlarm(View view) { // lahko mu dodaš argumente za čas etc nevem kko maš narejeno strukturo oziroma misliš izvest
        long diff_in_ms;
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        GregorianCalendar currentTime=new  GregorianCalendar (year,month,day,hour,minute,0);
        /**
         * TODO
         * menjaj podatke v GregorianCalendar targetTime=new  GregorianCalendar (2017,0,5,17,28,0); z podatki katere vpiše vporabnik
         * pazi ker datum je od 0-11 (0 = jan, 11 = dec)
         * podatki morajo biti v sledečem vrstnem redu leto, mesec, dan, ura, minuta in potem je 0 na koncu
         * mogoče boš moral popraviti bazo odvisno kko hraniš podatke etc nevem
         */
        int[] st=spacetime();
        GregorianCalendar targetTime=new  GregorianCalendar (st[0],st[1],st[2],st[3],st[4],0);
        diff_in_ms=targetTime.getTimeInMillis()-currentTime.getTimeInMillis();

        /**
         * TODO
         * zamenjaj +1000 z +diff_in_ms
         * 1000 je samo za testirat da takoj dobiš obvestilo
         */
        // pokličemo obvestilo v trenutnem času + razliki v milisekundah od določenega časa eventa
        // če hočeš hitro testirat zamenjaj v vrstici pod tem komentarjem diff_in_ms z recimo 3000 (3 sekunde)
        Long alertTime = new GregorianCalendar().getTimeInMillis()+diff_in_ms;//diff_in_ms;

        // executamo alert reciever
        Intent alertIntent = new Intent(this, AlertReceiver.class);
        //Bundle bundle = new Bundle();
        alertIntent.putExtra("title",n.getTitle());
        alertIntent.putExtra("text",n.getText());
        alertIntent.putExtra("date",n.getDate());
        alertIntent.putExtra("time",n.getTime());
        alertIntent.putExtra("location",n.getLocation());
        //alertIntent.putExtras(bundle);
        // poskrbimo da se notification pojavi tudi če je app zaprt
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        // notification nastavimo
        alarmManager.set(AlarmManager.RTC_WAKEUP, alertTime,
                PendingIntent.getBroadcast(this, 1, alertIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT));

    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        yearFinal = i;
        monthFinal = i1+1;
        dayFinal = i2;

        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(Addnote.this, Addnote.this, hour, minute, DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        EditText date=(EditText)findViewById(R.id.date);
        hourFinal = i;
        minuteFinal = i1;

        /**
         * TODO
         * namesto da povozi "click to select location" naj se ura in datum vpišeta v pravilen date edit text in time edit text
         * popravi malo elemente/text v elementih da bo vredu zgledalo
         */

        date.setText(yearFinal+"/"+monthFinal+"/"+dayFinal+" "+hourFinal+":"+minuteFinal);
        //date.setText(yearFinal+"/"+monthFinal+"/"+dayFinal);
        //time.setText(hourFinal+":"+minuteFinal);


    }
    public int[] spacetime(){
        String a=n.getDate();
        String b=n.getTime();
        String[] da=a.split("/");
        String [] db=b.split(":");
        int[] tim=new int[5];
        tim[0]=Integer.parseInt(da[0]);
        tim[1]=Integer.parseInt(da[1])-1;
        tim[2]=Integer.parseInt(da[2]);
        tim[3]=Integer.parseInt(db[0]);
        tim[4]=Integer.parseInt(db[1]);
        return  tim;
    }
    public String[] parser(String a){
        String []b=a.split(" ");
        return b;
    }
    public void DBDelete(){
        DatabaseConnector db=new DatabaseConnector(this);
        db.deleteEvent(n.getId());
        Intent in=new Intent(Addnote.this,Notes.class);
        startActivity(in);
    }
}
