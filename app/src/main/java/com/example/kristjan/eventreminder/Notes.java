package com.example.kristjan.eventreminder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Notes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        GridView gv = (GridView) findViewById(R.id.gv);
        DatabaseConnector db=new DatabaseConnector(this);

        List<Note> notesList1 = db.NoteList(Methods.userid+""); //seznam eventov iz baze
        final List<Note> notesList2=sort(notesList1);
        List <String> notesList=pars(notesList2);

        // Create a new ArrayAdapter
        final ArrayAdapter<String> gridViewArrayAdapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1, notesList);

        // Data bind GridView with ArrayAdapter (String Array elements)
        gv.setAdapter(gridViewArrayAdapter);


        Button nn=(Button)findViewById(R.id.nnote);
        nn.setOnClickListener(new View.OnClickListener(){ // klik na gumb add note
            public void onClick(View v){
                Intent in=new Intent(Notes.this,Addnote.class);
                startActivity(in);
            }
        });
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() { // klik na element od grid viewa
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Note[] pos=position(notesList2);
                Note a=pos[position];
                Intent intent = new Intent(Notes.this, Addnote.class);
                Bundle bundle = new Bundle();
                bundle.putString("id",a.getId());
                bundle.putString("title",a.getTitle());
                bundle.putString("text",a.getText());
                bundle.putString("date",a.getDate());
                bundle.putString("time",a.getTime());
                bundle.putString("location",a.getLocation());
                intent.putExtras(bundle);
                startActivityForResult(intent, 1234);

            }
        });
    }
    public List pars(List<Note> notes){ // prilagajanje seznama
        List<String>ad=new ArrayList<String>();
        String a="";
        for(int i=0;i<notes.size();i++){
            Note no=notes.get(i);
            a+=no.getTitle()+"\n";
            a+=no.getTime()+"\n";
            a+=no.getDate()+"\n";
            ad.add(a);
            a="";
        }
        return ad;
    }
    public Note[] position(List<Note> list){ // pozicije notov v gridu
        Note[] pos=new Note[list.size()];
        for(int i=0;i<pos.length;i++){
            pos[i]=list.get(i);
        }
        return pos;
    }
    public List sort(List<Note> notes){
        List<Note>ad=new ArrayList<Note>();
        while(notes.size()>0){
            int max=0;
            Note max1=new Note("0","0","0","0");
            for(int j=0;j<notes.size();j++){
                Note no=notes.get(j);
                String[] date=no.getDate().split("/");
                String date1="";
                for(int k=0;k<date.length;k++){
                    date1+=date[k];
                }
                int rem=Integer.parseInt(date1);
                if(rem>max){
                    max=rem;
                    max1=no;
                }
            }
            ad.add(max1);
            notes.remove(max1);
        }
        List<Note>jad=new ArrayList<Note>();
        for(int i=ad.size()-1;i>0;i--){
            jad.add(ad.get(i));
        }
        return jad;
    }
}
