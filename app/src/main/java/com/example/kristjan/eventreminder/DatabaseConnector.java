package com.example.kristjan.eventreminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseConnector extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "Events";
    private static final int DATABASE_VERSION = 1;

    // database object
    private SQLiteDatabase database;

    // database helper

    public DatabaseConnector(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Opens the database connection
     *
     * @throws SQLException
     */
    public void open() throws SQLException {
        database = this.getWritableDatabase();
    }
    public void onCreate(SQLiteDatabase db){ // ustvarjanje baz
        final String createTable1="create table users(" +
                "id integer primary key autoincrement," +
                "name TEXT," +
                "password TEXT" +
                ")";
        final String createTable2="create table events(" +
                "id integer primary key autoincrement," +
                "uid integer," +
                "text text,"+
                "title text," +
                "date text," +
                "time text," +
                "location text," +
                "FOREIGN KEY(uid)REFERENCES users(id)"+
                ")";
        db.execSQL(createTable1);
        db.execSQL(createTable2);
        //db.close();
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS events");
        onCreate(db);
    }
    public void adduser(String name,String password){ //dodajanje uporabnika
        String pass=Methods.MD5hash(password);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name",name);
        values.put("password",pass);
        db.insert("users",null,values);
        db.close();
    }
    public void addevent(Note note){ // dodajanje eventa
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("text",note.getText());
        values.put("title",note.getTitle());
        values.put("date",note.getDate());
        values.put("time",note.getTime());
        values.put("location",note.getLocation());
        values.put("uid",Methods.userid);
        db.insert("events",null,values);
        db.close();
    }
    public List namesList(){ // seznam uporabnikov
        List namesList=new ArrayList<String>();
        String sel="Select name from users";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sel, null);
        if (cursor.moveToFirst()) {
            do {
                namesList.add(cursor.getString(0));
            }
            while (cursor.moveToNext());
            }
        db.close();
        return namesList;
    }
    public List NoteList(String id){ // seznam eventov
        List noteList=new ArrayList<Note>();
        String sel="Select title,text,date,time,location,id from events where uid="+id;
        //Note ad="";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sel, null);
        if (cursor.moveToFirst()) {
            do {
                Note ad=new Note(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3));
                ad.setLoc(cursor.getString(4));
                ad.setId(cursor.getString(5));
                noteList.add(ad);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return noteList;
    }
    public Map<String,String> passwords(){ // gesla uporabnikov
        Map<String, String> dict = new HashMap<String, String>();
        String sel="Select name,password from users";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sel, null);
        if (cursor.moveToFirst()) {
            do {
                dict.put(cursor.getString(0),cursor.getString(1));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return dict;
    }
    public Map<String,String> ids(){ // id-ji uporabnikov
        Map<String, String> dict = new HashMap<String, String>();
        String sel="Select id,name from users";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sel, null);
        if (cursor.moveToFirst()) {
            do {
                dict.put(cursor.getString(1),cursor.getString(0));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return dict;
    }
    public void updateEvent(Note note){ //posodabljanje eventov
        SQLiteDatabase db = this.getWritableDatabase();
        String id=note.getId();
        ContentValues values = new ContentValues();
        values.put("text",note.getText());
        values.put("title",note.getTitle());
        values.put("date",note.getDate());
        values.put("time",note.getTime());
        values.put("location",note.getLocation());
        open();
        database.update("events", values, "id=" + id, null);
        close();

    }
    public void deleteEvent(String id) {
        open();
        database.delete("events", "id=" + id, null);
        close();
    }


    /**
     * Closes the database connection
     */
    public void close() {
        if (database != null) database.close(); // close the database connection
    }


}
