package com.example.kristjan.eventreminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.EditText;

public class AlertReceiver extends BroadcastReceiver{
    String title ;
    String text;
    String time ;
    String date ;
    String location ;
    // poklicana kadar je narejen broadcast ki cilja to metodo

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        title = extras.getString("title");
        text = extras.getString("text");
        time = extras.getString("time");
        date = extras.getString("date");
        location = extras.getString("location");




        /**
         * TODO OPTIONAL
         * zamenjaj "event is up" z imenom eventa (event title) in "click to see details" z čimr hočeš lahko recimo lokacijo etc
         */
        createNotification(context,title, date, "Alert");

    }

    public void createNotification(Context context, String msg, String msgText, String msgAlert){ // dodaj argumente za title time etc če jih rabiš nevem kko imaš naštimano

        // naredimo intent ki bo odprl MoreInfoNotification class in ji podal podatke od eventa, ki se bojo nato prikazali uporabniku
        Intent intent = new Intent(context, MoreInfoNotification.class );
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        /**
         * TODO
         * zamenjaj testne podatke z tistimi ki jih user vnese
         */

        intent.putExtra("title", title);
        intent.putExtra("time", time);
        intent.putExtra("date", date);
        intent.putExtra("text", text);
        intent.putExtra("location", location);

        PendingIntent notificIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // naredi notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.ic_menu_edit)
                        .setContentTitle(msg)
                        .setTicker(msgAlert)
                        .setContentText(msgText);

        // naredi intent za klik na notification
        mBuilder.setContentIntent(notificIntent);

        // določimo zven
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);

        // prekliče notification of kliku nanj
        mBuilder.setAutoCancel(true);

        // naredimo notification manager, ki bo obvestil uporabnika za event (ki se zgodi v ozadju)
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // 'poženemo' notification
        mNotificationManager.notify(1, mBuilder.build());

    }
}